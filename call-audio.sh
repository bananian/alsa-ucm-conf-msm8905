#!/bin/sh

set_ports () {
  pactl set-sink-port alsa_output.platform-sound.71.Voice__hw_msm8909skubsndc_27__sink "[Out] Voice$1"
  pactl set-source-port alsa_input.platform-sound.71.Voice__hw_msm8909skubsndc_27__source "[In] Voice$2"
}

workaround () {
    # Without this workaround, the phone doesn't always transmit audio
  amixer -D hw:msm8909skubsndc cset "name='VoiceMMode1_Tx Mixer TERT_MI2S_TX_MMode1'" 0
  amixer -D hw:msm8909skubsndc cset "name='VoiceMMode1_Tx Mixer TERT_MI2S_TX_MMode1'" 1
}

echo "**DEBUG** $1 executing"
case "$1" in
  audio_route)
    pactl set-card-profile 0 Voice
    set_ports Earpiece HandsetMic
    pacat >/dev/null 2>&1 </dev/zero &
    echo $! > /tmp/bananui-callui-$PPID.pacat.pid
    pamon >/dev/null 2>&1 &
    echo $! > /tmp/bananui-callui-$PPID.pamon.pid
    workaround
    ;;
  audio_unroute)
    pactl set-card-profile 0 HiFi
    kill $(cat /tmp/bananui-callui-$PPID.pacat.pid)
    kill $(cat /tmp/bananui-callui-$PPID.pamon.pid)
    rm /tmp/bananui-callui-$PPID.pacat.pid /tmp/bananui-callui-$PPID.pamon.pid
    ;;
  enable_speaker)
    set_ports Speaker MemsMic
    workaround
    ;;
  disable_speaker)
    set_ports Earpiece HandsetMic
    workaround
    ;;
  mute_tx)
    # Muting the TX is more complicated, so just mute the microphone
    amixer -D hw:msm8909skubsndc cset "name='DEC1 Volume'" 0
    amixer -D hw:msm8909skubsndc cset "name='DEC2 Volume'" 0
    ;;
  unmute_tx)
    # Volume default that nobody's expected to change (yet)
    amixer -D hw:msm8909skubsndc cset "name='DEC1 Volume'" 84
    amixer -D hw:msm8909skubsndc cset "name='DEC2 Volume'" 84
    ;;
  *)
    echo "This command should be run by bananui-callui" >&2
    exit 1
    ;;
esac
echo "**DEBUG** $1 finished"

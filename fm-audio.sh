#!/bin/sh

set_output () {
  pactl set-sink-port alsa_output.platform-sound.71.Radio__hw_msm8909skubsndc_0__sink "[Out] Radio$1"
}

echo "**DEBUG** $1 executing"
case "$1" in
  audio_route)
    pactl set-card-profile 0 Radio
    set_output Headphones
    hostless-routing-daemon /dev/snd/pcmC0D6c /dev/snd/pcmC0D5p &
    echo $! > /tmp/bananui-fm-$PPID.daemon.pid
    ;;
  audio_unroute)
    pactl set-card-profile 0 HiFi
    kill $(cat /tmp/bananui-fm-$PPID.daemon.pid)
    rm /tmp/bananui-fm-$PPID.daemon.pid
    ;;
  enable_speaker)
    set_output Speaker
    ;;
  disable_speaker)
    set_output Headphones
    ;;
  *)
    echo "This command should be run by bananui-callui" >&2
    exit 1
    ;;
esac
echo "**DEBUG** $1 finished"

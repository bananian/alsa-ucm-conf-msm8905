#!/bin/sh

set_output () {
  pactl set-sink-port alsa_output.platform-sound.71.HiFi__hw_msm8909skubsndc_0__sink "[Out] $1"
}
set_input () {
  pactl set-source-port alsa_input.platform-sound.71.HiFi__hw_msm8909skubsndc_0__source "[In] $1" || (echo "Wrong profile?"; kill $$)
}

echo "**DEBUG** $1 executing"
case "$1" in
  audio_route)
    set_input RadioReceiver
    hostless-routing-daemon /dev/snd/pcmC0D6c /dev/snd/pcmC0D5p &
    echo $! > /tmp/bananui-fm-$PPID.daemon.pid
    pactl /dev/null
    ;;
  audio_unroute)
    set_input MemsMic # TODO: save the previous input and restore it here
    kill $(cat /tmp/bananui-fm-$PPID.daemon.pid)
    rm /tmp/bananui-callui-$PPID.daemon.pid
    ;;
  *)
    echo "This command should be run by bananui-callui" >&2
    exit 1
    ;;
esac
echo "**DEBUG** $1 finished"
